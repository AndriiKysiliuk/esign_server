var express = require('express');
var bodyParser = require('body-parser');

var signer = require('./main.js');

var app = express();
var jsonParser = bodyParser.json()


app.post('/signXml', jsonParser, function(req, res) {
    const data = req.body;

    const signedFilePath = signer.SignDataInternal(data);

    res.status(200).json({path: signedFilePath}).send();
});

app.listen(3001);