//=============================================================================

/* Підключення бібліотек JavaScript */
var fs = require('fs');
eval(fs.readFileSync('./lib/euscpt.js')+'');
eval(fs.readFileSync('./lib/euscpm.js')+'');
eval(fs.readFileSync('./lib/euscp.js')+'');

//=============================================================================

/* Масив з шляхом до кореневих сертификатів ЦЗО та ЦСК */
var g_CACerts = [
	"./data/CACertificates.p7b",
];

/* Налаштування ЦСК за замовчанням */
var g_CADefaultSettings = {
	"issuerCNs": 				["Акредитований центр сертифікації ключів ІДД ДФС",
								"Акредитований центр сертифікації ключів ІДД Міндоходів",
								"Акредитований центр сертифікації ключів ІДД ДПС"],
	"address": 					"acskidd.gov.ua",
	"ocspAccessPointAddress":	"acskidd.gov.ua/services/ocsp/",
	"ocspAccessPointPort":		"80",
	"cmpAddress":				"acskidd.gov.ua",
	"tspAddress":				"acskidd.gov.ua",
	"tspAddressPort":			"80",
	"directAccess":				true
};

//-----------------------------------------------------------------------------

var g_euSign = EUSignCP();

//=============================================================================

/* Ініціалізація налаштувань криптографічної бібліотеки */
function SetSettings(caSettings) {
	var offline = true;
	var useOCSP = false;
	var useCMP = false;

	offline = ((caSettings == null) || (caSettings.address === ""));
	useOCSP = (!offline && (caSettings.ocspAccessPointAddress !== ""));
	useCMP = (!offline && (caSettings.cmpAddress !== ""));

	g_euSign.SetJavaStringCompliant(true);

	var settings = g_euSign.CreateFileStoreSettings();
	settings.SetPath('');
	settings.SetSaveLoadedCerts(false);
	g_euSign.SetFileStoreSettings(settings);

	settings = g_euSign.CreateModeSettings();
	settings.SetOfflineMode(offline);
	g_euSign.SetModeSettings(settings);

	settings = g_euSign.CreateProxySettings();
	g_euSign.SetProxySettings(settings);

	settings = g_euSign.CreateTSPSettings();
	settings.SetGetStamps(!offline);
	if (!offline) {
		if (caSettings.tspAddress !== "") {
			settings.SetAddress(caSettings.tspAddress);
			settings.SetPort(caSettings.tspAddressPort);
		} else if (g_CADefaultSettings){
			settings.SetAddress(g_CADefaultSettings.tspAddress);
			settings.SetPort(g_CADefaultSettings.tspAddressPort);
		}
	}
	g_euSign.SetTSPSettings(settings);

	settings = g_euSign.CreateOCSPSettings();
	if (useOCSP) {
		settings.SetUseOCSP(true);
		settings.SetBeforeStore(true);
		settings.SetAddress(caSettings.ocspAccessPointAddress);
		settings.SetPort("80");
	}
	g_euSign.SetOCSPSettings(settings);

	settings = g_euSign.CreateOCSPAccessInfoModeSettings();
	settings.SetEnabled(true);
	g_euSign.SetOCSPAccessInfoModeSettings(settings);
	settings = g_euSign.CreateOCSPAccessInfoSettings();


	settings.SetAddress(caSettings.ocspAccessPointAddress);
	settings.SetPort(caSettings.ocspAccessPointPort);
	for (var i = 0; i < caSettings.issuerCNs.length; i++) {
		settings.SetIssuerCN(caSettings.issuerCNs[i]);
		g_euSign.SetOCSPAccessInfoSettings(settings);
	}

	settings = g_euSign.CreateCMPSettings();
	settings.SetUseCMP(useCMP);
	if (useCMP) {
		settings.SetAddress(caSettings.cmpAddress);
		settings.SetPort("80");
	}
	g_euSign.SetCMPSettings(settings);

	settings = g_euSign.CreateLDAPSettings();
	g_euSign.SetLDAPSettings(settings);
}

//-----------------------------------------------------------------------------

/* Імпорт сертифікатів до сховища криптографічної бібліотеки */
function LoadCertificates(certsFilePathes) {
	if (!certsFilePathes)
		return;

	for (var i = 0; i < certsFilePathes.length; i++) {
		var path = certsFilePathes[i];
		var data = new Uint8Array(fs.readFileSync(path));
		if (path.substr(path.length - 3) == 'p7b') {
			g_euSign.SaveCertificates(data);
		} else {
			g_euSign.SaveCertificate(data);
		}
	}
}

//-----------------------------------------------------------------------------

/* Зчитування особистого ключа */
/* Ос. ключ використовується в функціях накладання підпису, зашифрування та */
/* розшифрування даних */
var ReadPrivateKey = function (pKeyFilePath, password, certsFilePathes) {
	/* Імпорт сертифікатів ос. ключа */
	LoadCertificates(certsFilePathes);
	/* Зчитування ключа */
	var pKeyData = new Uint8Array(fs.readFileSync(pKeyFilePath));
	g_euSign.ReadPrivateKeyBinary(pKeyData, password)
}

//-----------------------------------------------------------------------------

/* Ініціалізація криптографічної бібліотеки та встановлення налаштувань */
function Initialize(caSettings, key) {
	/* Ініціалізація криптографічної бібліотеки */
	g_euSign.Initialize();

	/* Встановлення параметрів за замовчанням */
	SetSettings(caSettings);

	/* Завантаження сертифікатів ЦСК */
	LoadCertificates(g_CACerts);

	/* Перевірка чи зчитано ос. ключ */
	/* Зчитування ос. ключа */
	ReadPrivateKey(key.filePath, key.password, key.certificates);
}

//=============================================================================

/* Формування зовнішнього підпису (дані та підпис знаходяться окремо) від */
/* даних типу Uint8Array. Функція повертає підпис у вигляді base64 */
/* У разі виникнення помилки, буде виключення */
function SignData(data) {
	// if (!g_isLibraryLoaded)
	// 	throw "Library not loaded";

	Initialize(true);
	return g_euSign.SignData(data, true);
}

//-----------------------------------------------------------------------------

/* Формування внутрішнього підпису (дані та підпис знаходяться разом) від */
/* даних типу Uint8Array. Функція повертає підписані дані у вигляді base64 */
/* У разі виникнення помилки, буде виключення */
var SignDataInternal = function (data) {
	const caSettings = data.caSettings;
	const key = data.keyConfig;
	try {
		Initialize(caSettings, key);
		const xmlBuff = Buffer.from(data.xml);
		g_euSign.SetRuntimeParameter(EU_SIGN_INCLUDE_CONTENT_TIME_STAMP_PARAMETER, false);
		g_euSign.SetRuntimeParameter(EU_SIGN_TYPE_PARAMETER, EU_SIGN_TYPE_CADES_T);
		const signature = g_euSign.SignDataInternal(true, xmlBuff, true);
		const buff = Buffer.from(signature, 'base64');
		const path = `signed_files/${new Date().getTime()}_signed_internal.xml`;
		fs.writeFileSync(path, buff);
		g_euSign.Finalize();
		return `${__dirname}/${path}`;
	} catch(e) {
		console.log(e);
	}
}

//-----------------------------------------------------------------------------

/* Перевірка зовнішнього підпису (дані та підпис знаходяться окремо) від */
/* даних типу Uint8Array. Функція повертає інф. про підпис у вигляді */
/* об'єкту типу EndUserSignInfo */
/* У разі виникнення помилки, буде виключення */
function VerifyData(data, sign) {
	if (!g_isLibraryLoaded)
		throw "Library not loaded";

	Initialize(false);

	return g_euSign.VerifyData(data, sign);
}

//-----------------------------------------------------------------------------

/* Перевірка внутрішнього підпису (дані та підпис знаходяться разом). */ 
/* Функція повертає інф. про підпис та дані, що підписувалися у вигляді */
/* об'єкту типу EndUserSignInfo */
/* У разі виникнення помилки, буде виключення */
function VerifyDataInternal(sign) {
	if (!g_isLibraryLoaded)
		throw "Library not loaded";

	Initialize(false);

	return g_euSign.VerifyDataInternal(sign);
}

//=============================================================================

/* Функція викликається після завантаження бібліотеки */
/* Функції бібліотеки можна викликати тільки після виклику EUSignCPModuleInitialized */
var EUSignCPModuleInitialized = function(isInitialized) {
	console.log('Library initialized');
}

module.exports = {
	SignDataInternal
};
